﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class PUNTAJE
    {

        private int valor;

        public int Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        private string descripcion;

        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        internal void EstablecerPuntaje(int valor, List<DADO> dados, int cantidadTiros)
        {
            
            switch (valor)
            {
                case 1:
                    descripcion = "1";
                    Calcular(1, dados);
                    break;

                case 2:
                    descripcion = "2";
                    Calcular(2, dados);
                    break;

                case 3:
                    descripcion = "3";
                    Calcular(3, dados);
                    break;

                case 4:
                    descripcion = "4";
                    Calcular(4, dados);
                    break;

                case 5:
                    descripcion = "5";
                    Calcular(5, dados);
                    break;

                case 6:
                    descripcion = "6";
                    Calcular(6, dados);
                    break;

                case 7:
                    descripcion = "ESCALERA";
                    Calcular(20, cantidadTiros);
                    break;

                case 8:
                    descripcion = "FULL";
                    Calcular(30, cantidadTiros);
                    break;

                case 9:
                    descripcion = "POKER";
                    Calcular(40, cantidadTiros);
                    break;

                case 10:
                    descripcion = "GENERALA";
                    Calcular(60, cantidadTiros);
                    break;

                default:
                    descripcion = "DOBLE GENERALA";
                    Calcular(100, cantidadTiros);
                    break;
                
            }
        }


        private void Calcular(int numero, List<DADO> dados)
        {
            foreach (DADO d in dados)
            {
                if (d.Valor == numero)
                {
                    valor += d.Valor;
                }
            }
        }

        private void Calcular(int v, int cantTiros)
        {
            if (cantTiros == 1)
            {
                valor = v + 5; 
            }
        }


    }
}