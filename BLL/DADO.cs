﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class DADO
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private int valor;

        public int Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        private POSICION pocision;

        public POSICION Pocision
        {
            get { return pocision; }
            set { pocision = value; }
        }

        private bool tiradenuevo = false;

        public bool TiraDeNuevo
        {
            get { return tiradenuevo; }
            set { tiradenuevo = value; }
        }


    }
}