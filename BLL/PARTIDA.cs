﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class PARTIDA
    {
        private List<JUGADOR> jugadores= new List<JUGADOR>();

        public List<JUGADOR> Jugadores
        {
            get { return jugadores; }
            set { jugadores = value; }
        }
        
    }
}