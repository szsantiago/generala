﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class POSICION
    {
        public POSICION() { }

        public POSICION(int xx, int yy)
        {
            x = xx;
            y = yy;
        }


        private int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }

        private int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

    }
}