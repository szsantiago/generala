﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class JUGADOR
    {

        public delegate void delEnviarDado(List<DADO> d);

        public event delEnviarDado EnviarDado;

        private int ID;

        public int id
        {
            get { return ID; }
            set { ID = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private DateTime tiempojugado;

        public DateTime TiempoJugado
        {
            get { return tiempojugado; }
            set { tiempojugado = value; }
        }

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        public void Tirar()
        {
            List<DADO> dados = TABLERO.Dados;

            List<DADO> dadosNuevos = new List<DADO>();

            foreach (DADO d in dados)
            {
                if (d.TiraDeNuevo)
                {
                    dadosNuevos.Add(d);
                }
            }


            if (dadosNuevos.Count == 0 || dadosNuevos.Count == 5)
            {
                dados.Clear();

                for (int da = 1; da <= 5; da++)
                {
                    POSICION posc = new POSICION(da, 10);

                    DADO d = new DADO();
                    d.ID = da;
                    d.Valor = TABLERO.random.Next(1, 7);
                    d.Pocision = posc;
                    d.TiraDeNuevo = false;
                    dados.Add(d);
                }

                
            }
            else
            {
                foreach (DADO d in dadosNuevos)
                {
                    DADO dadonuevo = (from DADO dad in dados
                                      where d == dad
                                      select dad
                                       ).FirstOrDefault();

                    dadonuevo.Valor = TABLERO.random.Next(1, 7);
                    dadonuevo.TiraDeNuevo = false;

                }
            }

            TABLERO.Dados = dados;

            this.EnviarDado(TABLERO.Dados);
        }

    }
}