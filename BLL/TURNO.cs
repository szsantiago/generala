﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class TURNO
    {
        private PUNTAJE puntaje;

        public PUNTAJE PUNTAJE
        {
            get { return puntaje; }
            set { puntaje = value; }
        }

        private JUGADOR jugador;

        public JUGADOR JUGADOR
        {
            get { return jugador; }
            set { jugador = value; }
        }

        private int tirosRestantes = 3;

        public int TirosRestantes
        {
            get { return tirosRestantes; }
            set { tirosRestantes = value; }
        }


        public void TerminaTurno(int puntajeelegido)
        {
            puntaje = new PUNTAJE();
            puntaje.EstablecerPuntaje(puntajeelegido, TABLERO.Dados, 3 - tirosRestantes);
            jugador.Puntos += puntaje.Valor;
            tirosRestantes = 0;
        }
    }
}