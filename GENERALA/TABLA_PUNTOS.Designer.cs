﻿namespace GENERALA
{
    partial class TABLA_PUNTOS
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.buttonEscalera = new System.Windows.Forms.Button();
            this.buttonFull = new System.Windows.Forms.Button();
            this.buttonPoker = new System.Windows.Forms.Button();
            this.buttonGenerala = new System.Windows.Forms.Button();
            this.buttonDobleGenerala = new System.Windows.Forms.Button();
            this.PUNTAJEJ1 = new System.Windows.Forms.Label();
            this.PUNTAJEJ2 = new System.Windows.Forms.Label();
            this.J1P1 = new System.Windows.Forms.TextBox();
            this.J1P2 = new System.Windows.Forms.TextBox();
            this.J1P3 = new System.Windows.Forms.TextBox();
            this.J1P4 = new System.Windows.Forms.TextBox();
            this.J1P5 = new System.Windows.Forms.TextBox();
            this.J1P6 = new System.Windows.Forms.TextBox();
            this.J1E = new System.Windows.Forms.TextBox();
            this.J1F = new System.Windows.Forms.TextBox();
            this.J1P = new System.Windows.Forms.TextBox();
            this.J1G = new System.Windows.Forms.TextBox();
            this.J1DG = new System.Windows.Forms.TextBox();
            this.J2P1 = new System.Windows.Forms.TextBox();
            this.J2P2 = new System.Windows.Forms.TextBox();
            this.J2P3 = new System.Windows.Forms.TextBox();
            this.J2P4 = new System.Windows.Forms.TextBox();
            this.J2P5 = new System.Windows.Forms.TextBox();
            this.J2P6 = new System.Windows.Forms.TextBox();
            this.J2E = new System.Windows.Forms.TextBox();
            this.J2F = new System.Windows.Forms.TextBox();
            this.J2P = new System.Windows.Forms.TextBox();
            this.J2G = new System.Windows.Forms.TextBox();
            this.J2DG = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GENERALA.Properties.Resources._7;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(294, 420);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button1.Location = new System.Drawing.Point(184, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "100";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button2.Location = new System.Drawing.Point(184, 74);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "100";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button3.Location = new System.Drawing.Point(184, 103);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "100";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button4.Location = new System.Drawing.Point(184, 132);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "100";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button5.Location = new System.Drawing.Point(184, 161);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(33, 23);
            this.button5.TabIndex = 5;
            this.button5.Text = "100";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.SystemColors.ControlLight;
            this.button6.Location = new System.Drawing.Point(184, 190);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(33, 23);
            this.button6.TabIndex = 6;
            this.button6.Text = "100";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // buttonEscalera
            // 
            this.buttonEscalera.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonEscalera.Location = new System.Drawing.Point(184, 221);
            this.buttonEscalera.Name = "buttonEscalera";
            this.buttonEscalera.Size = new System.Drawing.Size(33, 23);
            this.buttonEscalera.TabIndex = 7;
            this.buttonEscalera.Text = "100";
            this.buttonEscalera.UseVisualStyleBackColor = false;
            this.buttonEscalera.Click += new System.EventHandler(this.buttonEscalera_Click);
            // 
            // buttonFull
            // 
            this.buttonFull.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonFull.Location = new System.Drawing.Point(184, 254);
            this.buttonFull.Name = "buttonFull";
            this.buttonFull.Size = new System.Drawing.Size(33, 23);
            this.buttonFull.TabIndex = 8;
            this.buttonFull.Text = "100";
            this.buttonFull.UseVisualStyleBackColor = false;
            this.buttonFull.Click += new System.EventHandler(this.buttonFull_Click);
            // 
            // buttonPoker
            // 
            this.buttonPoker.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonPoker.Location = new System.Drawing.Point(184, 286);
            this.buttonPoker.Name = "buttonPoker";
            this.buttonPoker.Size = new System.Drawing.Size(33, 23);
            this.buttonPoker.TabIndex = 9;
            this.buttonPoker.Text = "100";
            this.buttonPoker.UseVisualStyleBackColor = false;
            this.buttonPoker.Click += new System.EventHandler(this.buttonPoker_Click);
            // 
            // buttonGenerala
            // 
            this.buttonGenerala.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonGenerala.Location = new System.Drawing.Point(184, 319);
            this.buttonGenerala.Name = "buttonGenerala";
            this.buttonGenerala.Size = new System.Drawing.Size(33, 23);
            this.buttonGenerala.TabIndex = 10;
            this.buttonGenerala.Text = "100";
            this.buttonGenerala.UseVisualStyleBackColor = false;
            this.buttonGenerala.Click += new System.EventHandler(this.buttonGenerala_Click);
            // 
            // buttonDobleGenerala
            // 
            this.buttonDobleGenerala.BackColor = System.Drawing.SystemColors.ControlLight;
            this.buttonDobleGenerala.Location = new System.Drawing.Point(184, 350);
            this.buttonDobleGenerala.Name = "buttonDobleGenerala";
            this.buttonDobleGenerala.Size = new System.Drawing.Size(33, 23);
            this.buttonDobleGenerala.TabIndex = 11;
            this.buttonDobleGenerala.Text = "100";
            this.buttonDobleGenerala.UseVisualStyleBackColor = false;
            this.buttonDobleGenerala.Click += new System.EventHandler(this.buttonDobleGenerala_Click);
            // 
            // PUNTAJEJ1
            // 
            this.PUNTAJEJ1.AutoSize = true;
            this.PUNTAJEJ1.BackColor = System.Drawing.Color.White;
            this.PUNTAJEJ1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PUNTAJEJ1.Location = new System.Drawing.Point(128, 388);
            this.PUNTAJEJ1.MaximumSize = new System.Drawing.Size(45, 25);
            this.PUNTAJEJ1.MinimumSize = new System.Drawing.Size(45, 25);
            this.PUNTAJEJ1.Name = "PUNTAJEJ1";
            this.PUNTAJEJ1.Size = new System.Drawing.Size(45, 25);
            this.PUNTAJEJ1.TabIndex = 12;
            this.PUNTAJEJ1.Text = "  0";
            // 
            // PUNTAJEJ2
            // 
            this.PUNTAJEJ2.AutoSize = true;
            this.PUNTAJEJ2.BackColor = System.Drawing.Color.White;
            this.PUNTAJEJ2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PUNTAJEJ2.Location = new System.Drawing.Point(228, 388);
            this.PUNTAJEJ2.MaximumSize = new System.Drawing.Size(45, 25);
            this.PUNTAJEJ2.MinimumSize = new System.Drawing.Size(45, 25);
            this.PUNTAJEJ2.Name = "PUNTAJEJ2";
            this.PUNTAJEJ2.Size = new System.Drawing.Size(45, 25);
            this.PUNTAJEJ2.TabIndex = 14;
            this.PUNTAJEJ2.Text = "  0";
            // 
            // J1P1
            // 
            this.J1P1.BackColor = System.Drawing.Color.Black;
            this.J1P1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1P1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1P1.ForeColor = System.Drawing.Color.White;
            this.J1P1.Location = new System.Drawing.Point(133, 48);
            this.J1P1.Name = "J1P1";
            this.J1P1.ReadOnly = true;
            this.J1P1.Size = new System.Drawing.Size(34, 21);
            this.J1P1.TabIndex = 15;
            this.J1P1.Text = "30";
            this.J1P1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1P2
            // 
            this.J1P2.BackColor = System.Drawing.Color.Black;
            this.J1P2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1P2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1P2.ForeColor = System.Drawing.Color.White;
            this.J1P2.Location = new System.Drawing.Point(133, 77);
            this.J1P2.Name = "J1P2";
            this.J1P2.ReadOnly = true;
            this.J1P2.Size = new System.Drawing.Size(34, 21);
            this.J1P2.TabIndex = 16;
            this.J1P2.Text = "30";
            this.J1P2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1P3
            // 
            this.J1P3.BackColor = System.Drawing.Color.Black;
            this.J1P3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1P3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1P3.ForeColor = System.Drawing.Color.White;
            this.J1P3.Location = new System.Drawing.Point(133, 106);
            this.J1P3.Name = "J1P3";
            this.J1P3.ReadOnly = true;
            this.J1P3.Size = new System.Drawing.Size(34, 21);
            this.J1P3.TabIndex = 17;
            this.J1P3.Text = "30";
            this.J1P3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1P4
            // 
            this.J1P4.BackColor = System.Drawing.Color.Black;
            this.J1P4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1P4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1P4.ForeColor = System.Drawing.Color.White;
            this.J1P4.Location = new System.Drawing.Point(133, 135);
            this.J1P4.Name = "J1P4";
            this.J1P4.ReadOnly = true;
            this.J1P4.Size = new System.Drawing.Size(34, 21);
            this.J1P4.TabIndex = 18;
            this.J1P4.Text = "30";
            this.J1P4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1P5
            // 
            this.J1P5.BackColor = System.Drawing.Color.Black;
            this.J1P5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1P5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1P5.ForeColor = System.Drawing.Color.White;
            this.J1P5.Location = new System.Drawing.Point(133, 164);
            this.J1P5.Name = "J1P5";
            this.J1P5.ReadOnly = true;
            this.J1P5.Size = new System.Drawing.Size(34, 21);
            this.J1P5.TabIndex = 19;
            this.J1P5.Text = "30";
            this.J1P5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1P6
            // 
            this.J1P6.BackColor = System.Drawing.Color.Black;
            this.J1P6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1P6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1P6.ForeColor = System.Drawing.Color.White;
            this.J1P6.Location = new System.Drawing.Point(133, 193);
            this.J1P6.Name = "J1P6";
            this.J1P6.ReadOnly = true;
            this.J1P6.Size = new System.Drawing.Size(34, 21);
            this.J1P6.TabIndex = 20;
            this.J1P6.Text = "30";
            this.J1P6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1E
            // 
            this.J1E.BackColor = System.Drawing.Color.Black;
            this.J1E.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1E.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1E.ForeColor = System.Drawing.Color.White;
            this.J1E.Location = new System.Drawing.Point(133, 224);
            this.J1E.Name = "J1E";
            this.J1E.ReadOnly = true;
            this.J1E.Size = new System.Drawing.Size(34, 21);
            this.J1E.TabIndex = 21;
            this.J1E.Text = "30";
            this.J1E.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1F
            // 
            this.J1F.BackColor = System.Drawing.Color.Black;
            this.J1F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1F.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1F.ForeColor = System.Drawing.Color.White;
            this.J1F.Location = new System.Drawing.Point(133, 257);
            this.J1F.Name = "J1F";
            this.J1F.ReadOnly = true;
            this.J1F.Size = new System.Drawing.Size(34, 21);
            this.J1F.TabIndex = 22;
            this.J1F.Text = "30";
            this.J1F.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1P
            // 
            this.J1P.BackColor = System.Drawing.Color.Black;
            this.J1P.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1P.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1P.ForeColor = System.Drawing.Color.White;
            this.J1P.Location = new System.Drawing.Point(133, 289);
            this.J1P.Name = "J1P";
            this.J1P.ReadOnly = true;
            this.J1P.Size = new System.Drawing.Size(34, 21);
            this.J1P.TabIndex = 23;
            this.J1P.Text = "30";
            this.J1P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1G
            // 
            this.J1G.BackColor = System.Drawing.Color.Black;
            this.J1G.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1G.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1G.ForeColor = System.Drawing.Color.White;
            this.J1G.Location = new System.Drawing.Point(133, 322);
            this.J1G.Name = "J1G";
            this.J1G.ReadOnly = true;
            this.J1G.Size = new System.Drawing.Size(34, 21);
            this.J1G.TabIndex = 24;
            this.J1G.Text = "30";
            this.J1G.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J1DG
            // 
            this.J1DG.BackColor = System.Drawing.Color.Black;
            this.J1DG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J1DG.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J1DG.ForeColor = System.Drawing.Color.White;
            this.J1DG.Location = new System.Drawing.Point(133, 353);
            this.J1DG.Name = "J1DG";
            this.J1DG.ReadOnly = true;
            this.J1DG.Size = new System.Drawing.Size(34, 21);
            this.J1DG.TabIndex = 25;
            this.J1DG.Text = "30";
            this.J1DG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2P1
            // 
            this.J2P1.BackColor = System.Drawing.Color.Black;
            this.J2P1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2P1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2P1.ForeColor = System.Drawing.Color.White;
            this.J2P1.Location = new System.Drawing.Point(233, 48);
            this.J2P1.Name = "J2P1";
            this.J2P1.ReadOnly = true;
            this.J2P1.Size = new System.Drawing.Size(34, 21);
            this.J2P1.TabIndex = 26;
            this.J2P1.Text = "30";
            this.J2P1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2P2
            // 
            this.J2P2.BackColor = System.Drawing.Color.Black;
            this.J2P2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2P2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2P2.ForeColor = System.Drawing.Color.White;
            this.J2P2.Location = new System.Drawing.Point(233, 77);
            this.J2P2.Name = "J2P2";
            this.J2P2.ReadOnly = true;
            this.J2P2.Size = new System.Drawing.Size(34, 21);
            this.J2P2.TabIndex = 27;
            this.J2P2.Text = "30";
            this.J2P2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2P3
            // 
            this.J2P3.BackColor = System.Drawing.Color.Black;
            this.J2P3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2P3.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2P3.ForeColor = System.Drawing.Color.White;
            this.J2P3.Location = new System.Drawing.Point(233, 106);
            this.J2P3.Name = "J2P3";
            this.J2P3.ReadOnly = true;
            this.J2P3.Size = new System.Drawing.Size(34, 21);
            this.J2P3.TabIndex = 28;
            this.J2P3.Text = "30";
            this.J2P3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2P4
            // 
            this.J2P4.BackColor = System.Drawing.Color.Black;
            this.J2P4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2P4.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2P4.ForeColor = System.Drawing.Color.White;
            this.J2P4.Location = new System.Drawing.Point(233, 135);
            this.J2P4.Name = "J2P4";
            this.J2P4.ReadOnly = true;
            this.J2P4.Size = new System.Drawing.Size(34, 21);
            this.J2P4.TabIndex = 29;
            this.J2P4.Text = "30";
            this.J2P4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2P5
            // 
            this.J2P5.BackColor = System.Drawing.Color.Black;
            this.J2P5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2P5.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2P5.ForeColor = System.Drawing.Color.White;
            this.J2P5.Location = new System.Drawing.Point(233, 164);
            this.J2P5.Name = "J2P5";
            this.J2P5.ReadOnly = true;
            this.J2P5.Size = new System.Drawing.Size(34, 21);
            this.J2P5.TabIndex = 30;
            this.J2P5.Text = "30";
            this.J2P5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2P6
            // 
            this.J2P6.BackColor = System.Drawing.Color.Black;
            this.J2P6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2P6.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2P6.ForeColor = System.Drawing.Color.White;
            this.J2P6.Location = new System.Drawing.Point(233, 193);
            this.J2P6.Name = "J2P6";
            this.J2P6.ReadOnly = true;
            this.J2P6.Size = new System.Drawing.Size(34, 21);
            this.J2P6.TabIndex = 31;
            this.J2P6.Text = "30";
            this.J2P6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2E
            // 
            this.J2E.BackColor = System.Drawing.Color.Black;
            this.J2E.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2E.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2E.ForeColor = System.Drawing.Color.White;
            this.J2E.Location = new System.Drawing.Point(233, 224);
            this.J2E.Name = "J2E";
            this.J2E.ReadOnly = true;
            this.J2E.Size = new System.Drawing.Size(34, 21);
            this.J2E.TabIndex = 32;
            this.J2E.Text = "30";
            this.J2E.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2F
            // 
            this.J2F.BackColor = System.Drawing.Color.Black;
            this.J2F.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2F.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2F.ForeColor = System.Drawing.Color.White;
            this.J2F.Location = new System.Drawing.Point(233, 257);
            this.J2F.Name = "J2F";
            this.J2F.ReadOnly = true;
            this.J2F.Size = new System.Drawing.Size(34, 21);
            this.J2F.TabIndex = 33;
            this.J2F.Text = "30";
            this.J2F.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2P
            // 
            this.J2P.BackColor = System.Drawing.Color.Black;
            this.J2P.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2P.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2P.ForeColor = System.Drawing.Color.White;
            this.J2P.Location = new System.Drawing.Point(233, 289);
            this.J2P.Name = "J2P";
            this.J2P.ReadOnly = true;
            this.J2P.Size = new System.Drawing.Size(34, 21);
            this.J2P.TabIndex = 34;
            this.J2P.Text = "30";
            this.J2P.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2G
            // 
            this.J2G.BackColor = System.Drawing.Color.Black;
            this.J2G.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2G.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2G.ForeColor = System.Drawing.Color.White;
            this.J2G.Location = new System.Drawing.Point(233, 322);
            this.J2G.Name = "J2G";
            this.J2G.ReadOnly = true;
            this.J2G.Size = new System.Drawing.Size(34, 21);
            this.J2G.TabIndex = 35;
            this.J2G.Text = "30";
            this.J2G.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // J2DG
            // 
            this.J2DG.BackColor = System.Drawing.Color.Black;
            this.J2DG.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.J2DG.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.J2DG.ForeColor = System.Drawing.Color.White;
            this.J2DG.Location = new System.Drawing.Point(233, 353);
            this.J2DG.Name = "J2DG";
            this.J2DG.ReadOnly = true;
            this.J2DG.Size = new System.Drawing.Size(34, 21);
            this.J2DG.TabIndex = 36;
            this.J2DG.Text = "30";
            this.J2DG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TABLA_PUNTOS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.J2DG);
            this.Controls.Add(this.J2G);
            this.Controls.Add(this.J2P);
            this.Controls.Add(this.J2F);
            this.Controls.Add(this.J2E);
            this.Controls.Add(this.J2P6);
            this.Controls.Add(this.J2P5);
            this.Controls.Add(this.J2P4);
            this.Controls.Add(this.J2P3);
            this.Controls.Add(this.J2P2);
            this.Controls.Add(this.J2P1);
            this.Controls.Add(this.J1DG);
            this.Controls.Add(this.J1G);
            this.Controls.Add(this.J1P);
            this.Controls.Add(this.J1F);
            this.Controls.Add(this.J1E);
            this.Controls.Add(this.J1P6);
            this.Controls.Add(this.J1P5);
            this.Controls.Add(this.J1P4);
            this.Controls.Add(this.J1P3);
            this.Controls.Add(this.J1P2);
            this.Controls.Add(this.J1P1);
            this.Controls.Add(this.PUNTAJEJ2);
            this.Controls.Add(this.PUNTAJEJ1);
            this.Controls.Add(this.buttonDobleGenerala);
            this.Controls.Add(this.buttonGenerala);
            this.Controls.Add(this.buttonPoker);
            this.Controls.Add(this.buttonFull);
            this.Controls.Add(this.buttonEscalera);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "TABLA_PUNTOS";
            this.Size = new System.Drawing.Size(301, 426);
            this.Load += new System.EventHandler(this.TABLA_PUNTOS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button buttonEscalera;
        private System.Windows.Forms.Button buttonFull;
        private System.Windows.Forms.Button buttonPoker;
        private System.Windows.Forms.Button buttonGenerala;
        private System.Windows.Forms.Button buttonDobleGenerala;
        private System.Windows.Forms.Label PUNTAJEJ1;
        private System.Windows.Forms.Label PUNTAJEJ2;
        private System.Windows.Forms.TextBox J1P1;
        private System.Windows.Forms.TextBox J1P2;
        private System.Windows.Forms.TextBox J1P3;
        private System.Windows.Forms.TextBox J1P4;
        private System.Windows.Forms.TextBox J1P5;
        private System.Windows.Forms.TextBox J1P6;
        private System.Windows.Forms.TextBox J1E;
        private System.Windows.Forms.TextBox J1F;
        private System.Windows.Forms.TextBox J1P;
        private System.Windows.Forms.TextBox J1G;
        private System.Windows.Forms.TextBox J1DG;
        private System.Windows.Forms.TextBox J2P1;
        private System.Windows.Forms.TextBox J2P2;
        private System.Windows.Forms.TextBox J2P3;
        private System.Windows.Forms.TextBox J2P4;
        private System.Windows.Forms.TextBox J2P5;
        private System.Windows.Forms.TextBox J2P6;
        private System.Windows.Forms.TextBox J2E;
        private System.Windows.Forms.TextBox J2F;
        private System.Windows.Forms.TextBox J2P;
        private System.Windows.Forms.TextBox J2G;
        private System.Windows.Forms.TextBox J2DG;
    }
}
