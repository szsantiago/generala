﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace GENERALA
{
    public partial class Form1 : Form
    {

        static TURNO turno = new TURNO();
        JUGADOR j = new JUGADOR();
        PARTIDA partida = new PARTIDA();

        public Form1()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {

            j.EnviarDado += T_EnviarDado;
            timer1.Enabled = true;
        }

        private void T_EnviarDado(List<BLL.DADO> d)
        {
            
            foreach (BLL.DADO DADO in d)
            {
                DADO dado = new DADO();
                dado.Width = 100;
                dado.Height = 100;
                dado.Location = new Point((DADO.Pocision.X * dado.Width), DADO.Pocision.Y +5);
                dado.EstablecerImagen(DADO.Valor);
                dado.DadoAsignado = DADO;
               
                this.Controls.Add(dado);
            }
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int indice = 0;

            List<Control> controles_Borrar = new List<Control>();

            foreach (Control c in Controls)
            {
                if (c is DADO)
                {
                    controles_Borrar.Add(c);
                }
                else
                {
                    indice++;
                }
            }

            foreach (Control c in controles_Borrar)
            {
                Controls.RemoveAt(indice);
            }


            if (turno.TirosRestantes > 1)
            {
                j.Tirar();
                turno.TirosRestantes--;   
            }
            else
            {
                j.Tirar();
                turno.TirosRestantes--;
                button1.Enabled = false;
            }

           

            tablA_PUNTOS1.MostrarOpcionesPuntaje(BLL.TABLERO.Dados, turno.TirosRestantes);
            
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            j.id = int.Parse(textBox1.Text);
            j.Nombre = textBox2.Text;

            partida.Jugadores.Add(j);
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tablA_PUNTOS1.Jugador1 = partida.Jugadores[0];
            tablA_PUNTOS1.Jugador2 = partida.Jugadores[1];
            
            turno.JUGADOR = tablA_PUNTOS1.Jugador1;
            turno.TirosRestantes = 3;
            tablA_PUNTOS1.Turno = turno;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (turno.TirosRestantes == 0)
            {
                button1.Enabled = false;
            }
        }
    }
}
