﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GENERALA
{
    public partial class DADO : UserControl
    {
        public DADO()
        {
            InitializeComponent();
        }

        private BLL.DADO dadoasignado;

        public BLL.DADO DadoAsignado
        {
            get { return dadoasignado; }
            set { dadoasignado = value; }
        }


        public void EstablecerImagen(int valor)
        {
            switch (valor)
            {
                case 1:
                    {
                        pictureBox1.Image = Image.FromFile(".\\IMAGENES\\1.png");
                        break;
                    }
                case 2:
                    {
                        pictureBox1.Image = Image.FromFile(".\\IMAGENES\\2.png");
                        break;
                    }
                case 3:
                    {
                        pictureBox1.Image = Image.FromFile(".\\IMAGENES\\3.png");
                        break;
                    }
                case 4:
                    {
                        pictureBox1.Image = Image.FromFile(".\\IMAGENES\\4.png");
                        break;
                    }
                case 5:
                    {
                        pictureBox1.Image = Image.FromFile(".\\IMAGENES\\5.png");
                        break;
                    }
                default:
                    {
                        pictureBox1.Image = Image.FromFile(".\\IMAGENES\\6.png");
                        break;
                    }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (dadoasignado.TiraDeNuevo)
            {
                dadoasignado.TiraDeNuevo = false;
                this.BackColor = System.Drawing.SystemColors.Control;
            }
            else
            {
                dadoasignado.TiraDeNuevo = true;
                this.BackColor = Color.Red;
            }
            
        }
    }
}

