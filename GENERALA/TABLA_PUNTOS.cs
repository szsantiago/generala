﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GENERALA
{
    public partial class TABLA_PUNTOS : UserControl
    {
        public TABLA_PUNTOS()
        {
            InitializeComponent();
        }

        private BLL.JUGADOR jugador1;

        public BLL.JUGADOR Jugador1
        {
            get { return jugador1; }
            set { jugador1 = value; }
        }

        private BLL.JUGADOR jugador2;

        public BLL.JUGADOR Jugador2
        {
            get { return jugador2; }
            set { jugador2 = value; }
        }

        private BLL.TURNO turno;

        public BLL.TURNO Turno
        {
            get { return turno; }
            set { turno = value; }
        }



        private void TABLA_PUNTOS_Load(object sender, EventArgs e)
        {
            foreach (Control c in Controls)
            {
                if (c is TextBox || c is Button)
                {
                    c.Visible = false;
                }
            }
        }
        
        private void EsconderBotones()
        {
            foreach (Control c in Controls)
            {
                if (c is Button)
                {
                    c.Visible = false;
                }
            }
        }
        

        public void MostrarOpcionesPuntaje(List<BLL.DADO> dados, int tirosRestantes)
        {

            List<int> valores = new List<int>();

            for (int i = 0; i < 6; i++)
            {
                valores.Add(0);
            }

            EsconderBotones();

            foreach (BLL.DADO d in dados)
            {
                switch (d.Valor)
                {
                    case 1:
                        valores[0]++;
                        if (!J1P1.Visible && turno.JUGADOR == jugador1)
                        {
                            button1.Text = valores[0].ToString();
                            button1.Visible = true;
                        }
                        break;
                    case 2:
                        valores[1]++;
                        if (!J1P2.Visible && turno.JUGADOR == jugador1)
                        {
                            button2.Text = (valores[1]*2).ToString();
                            button2.Visible = true;
                        }
                        break;
                    case 3:
                        valores[2]++;
                        if (!J1P3.Visible && turno.JUGADOR == jugador1)
                        {
                            button3.Text = (valores[2]*3).ToString();
                            button3.Visible = true;
                        }
                        break;
                    case 4:
                        valores[3]++;
                        if (!J1P4.Visible && turno.JUGADOR == jugador1)
                        {
                            button4.Text = (valores[3]*4).ToString();
                            button4.Visible = true;
                        }
                        break;
                    case 5:
                        valores[4]++;
                        if (!J1P5.Visible && turno.JUGADOR == jugador1)
                        {
                            button5.Text = (valores[4]*5).ToString();
                            button5.Visible = true;
                        }
                        break;
                    default:
                        valores[5]++;
                        if (!J1P6.Visible && turno.JUGADOR == jugador1)
                        {
                            button6.Text = (valores[5] * 6).ToString();
                            button6.Visible = true;
                        }
                        break;
                }
            }

            foreach (int i in valores)
            {
                if (i == 3 && !J1F.Visible)
                {
                    foreach (int d in valores)
                    {
                        if (d == 2)
                        {
                            if (tirosRestantes == 2)
                            {
                                buttonFull.Text = "35";
                            }
                            else
                            {
                                buttonFull.Text = "30";
                            }

                            buttonFull.Visible = true;
                        }
                    }
                }                               //FULL
                else if (i == 4 && !J1P.Visible)
                {
                    if (tirosRestantes == 2)
                    {
                        buttonPoker.Text = "45";
                    }
                    else
                    {
                        buttonPoker.Text = "40";
                    }

                    buttonPoker.Visible = true;
                }                          //POKER
                else if (i == 5 && !J1G.Visible)
                {
                    buttonGenerala.Visible = true;
                }
            }

        }

        #region<Botones>

        private void button1_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1P1.Text = button1.Text;
                J1P1.Visible = true;
            }
            else
            {
                J2P1.Text = button1.Text;
                J2P1.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(1);
            NuevoTurno();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1P2.Text = button2.Text;
                J1P2.Visible = true;
            }
            else
            {
                J2P2.Text = button2.Text;
                J2P2.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(2);
            NuevoTurno();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1P3.Text = button3.Text;
                J1P3.Visible = true;
            }
            else
            {
                J2P3.Text = button3.Text;
                J2P3.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(3);
            NuevoTurno();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1P4.Text = button4.Text;
                J1P4.Visible = true;
            }
            else
            {
                J2P4.Text = button4.Text;
                J2P4.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(4);
            NuevoTurno();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1P5.Text = button5.Text;
                J1P5.Visible = true;
            }
            else
            {
                J2P5.Text = button5.Text;
                J2P5.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(5);
            NuevoTurno();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1P6.Text = button6.Text;
                J1P6.Visible = true;
            }
            else
            {
                J2P6.Text = button6.Text;
                J2P6.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(6);
            NuevoTurno();
        }

        private void buttonEscalera_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1E.Text = buttonEscalera.Text;
                J1E.Visible = true;
            }
            else
            {
                J2E.Text = buttonEscalera.Text;
                J2E.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(7);
            NuevoTurno();
        }

        private void buttonFull_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1F.Text = buttonFull.Text;
                J1F.Visible = true;
            }
            else
            {
                J2F.Text = buttonFull.Text;
                J2F.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(8);
            NuevoTurno();
        }

        private void buttonPoker_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1P.Text = buttonPoker.Text;
                J1P.Visible = true;
            }
            else
            {
                J2P.Text = buttonPoker.Text;
                J2P.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(9);
            NuevoTurno();
        }

        private void buttonGenerala_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1G.Text = buttonGenerala.Text;
                J1G.Visible = true;
            }
            else
            {
                J2G.Text = buttonGenerala.Text;
                J2G.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(10);
            NuevoTurno();
        }

        private void buttonDobleGenerala_Click(object sender, EventArgs e)
        {
            if (turno.JUGADOR == jugador1)
            {
                J1DG.Text = buttonDobleGenerala.Text;
                J1DG.Visible = true;
            }
            else
            {
                J2DG.Text = buttonDobleGenerala.Text;
                J2DG.Visible = true;
            }
            EsconderBotones();
            turno.TerminaTurno(11);
            NuevoTurno();
        }

        #endregion

        private void NuevoTurno()
        {
            
            if (turno.TirosRestantes == 0)
            {
                turno = new BLL.TURNO();

                if (Turno.JUGADOR == Jugador1)
                {
                    turno.JUGADOR = Jugador2;
                }
                else
                {
                    turno.JUGADOR = Jugador1;
                }
                
                
            }
        }
    }
}
